import os

from setuptools import find_packages, setup

__version__ = "0.1.0"

current_directory = os.path.abspath(os.path.dirname(__file__))
readme_file_location = os.path.join(current_directory, "README.md")

with open(readme_file_location, encoding="utf-8") as readme_file:
    long_description = readme_file.read()

setup(
    name="cloudsmith-evaluation",
    version=__version__,
    description="cloudsmith-evaluation",
    long_description_content_type="text/markdown",
    long_description=long_description,
    author="developer",
    python_requires=">=3.8.1",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    platforms=["Linux", "Mac OS"],
    zip_safe=False,
)
