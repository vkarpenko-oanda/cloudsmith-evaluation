#!/usr/bin/env bash


VIRTUAL_ENV_NAME="venv"

[[ ! -d "venv" ]] && \
    echo "Could not find virtual environment. Creating..." && \
    python3.8 -m venv "$VIRTUAL_ENV_NAME";

[[ ! -n "${VIRTUAL_ENV}" ]] && \
    echo "No active virtual environment found. Activating..." && \
    source "$VIRTUAL_ENV_NAME/bin/activate";

if [[ -n "${VIRTUAL_ENV}" ]]; then
    "$VIRTUAL_ENV_NAME/bin/python" -m pip install --upgrade pip==21.2.3 wheel==0.37.0 setuptools==57.4.0
else
    echo "You must be in virtual environment in order to install packages.";
fi
